@extends('layout.master')

@section('title')
{{ env('TITLE') }} - User - Create
@stop

@section('content')
	<h1>Create User</h1>
	@include('users.form')
@stop