@extends('layout.master')

@section('title')
{{ env('TITLE') }} User - Edit
@stop

@section('content')
	<h1>Edit User</h1>
	@include('users.form')
@stop