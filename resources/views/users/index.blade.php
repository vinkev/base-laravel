@extends('layout.master')

@section('title')
	{{env('TITLE')}} - Users
@endsection

@section('content')
	<h1>List Users</h1>
	@if(Entrust::hasRole("admin"))
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="btn {{ env('BTN_ADD') }}" href="{{ action('UserController@create')}}">Add New User</a>
		</div>
	</div>
	@endif

	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<tr class="{{ env("TBL_HEAD") }}}">
					<th>Name</th>
					<th>Email</th>
                    <th>Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
				<tr>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>
                    <td>{{count($user->roles) > 0 ? $user->roles[0]->display_name : ''}}</td>
					<td class="col-sm-1 nowrap">
                        @if(Entrust::user() != null && (Entrust::user()->id == $user->id || Entrust::hasRole("admin")))
						<a class="btn {{ env('BTN_EDIT') }} btn-xs" href="{{ action('UserController@edit', $user->id) }}" >Edit</a>
                        @endif
			   			@if(Entrust::hasRole("admin"))
                        {!! Form::open(array('url' => action('UserController@destroy', $user->id), 'class' => 'form-delete')) !!}
			   			{!! Form::hidden('_method', 'delete') !!}
			        		<button type="submit" class="btn {{ env('BTN_DELETE' )}} btn-xs">Delete</button>
		    		    {!! Form::close() !!}
                        @endif
			    	</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('layout.pagination', ['page' => $users])
@stop