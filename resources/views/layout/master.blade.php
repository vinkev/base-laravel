<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		@section('title')
			{{ env('TITLE') }}
		@show
	</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
</head>
<body>
	@include('layout.header')
	<div id="tpl-content" class="container-fluid">
		<div class="row clearfix">
			<div class="col-md-2 sidebar">
				@include('layout.menu')
			</div>
			<div class="col-md-10 col-xs-12 pull-right">
				@yield('content')
			</div>
		</div>
	</div>
	@include('layout.footer')
</body>
</html>