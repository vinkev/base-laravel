@if (Auth::check())
<ul class="nav nav-pills nav-stacked" role="navigation">
	<li role="presentation" class="{{ strpos(Request::path(),'products') !== false ? 'active' : '' }}"><a href="{{}}"> <span class="glyphicon glyphicon-list"></span> Product</a></li>
	<li role="presentation"><a href=""> <span class="glyphicon glyphicon-list"></span> test</a></li>
	<li class="nav-header">User Management</li>
	<li role="presentation"><a href="{!! action('UserController@index') !!}"> <span class="glyphicon glyphicon-user"></span> User</a></li>

	@if(Entrust::hasRole('admin'))
	<li role="presentation"><a href="{!! action('RoleController@index') !!}"> <span class="glyphicon glyphicon-list-alt"></span> Role</a></li>
	@endif
</ul>
@endif