@extends('layout.master')

@section('title')
{{ env('TITLE') }} - Role - Create
@stop

@section('content')
	<h1>Create Roles</h1>
	@include('roles.form')
@stop