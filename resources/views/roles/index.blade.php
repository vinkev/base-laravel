@extends('layout.master')

@section('title')
{{ env('TITLE') }} - Roles
@stop

@section('content')
	<h1>List Roles</h1>
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="btn {{ env('BTN_ADD') }}" href="{{ action('RoleController@create')}}">Add New Role</a>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<tr class="{{ env("TBL_HEAD") }}}">
					<th>Name</th>
					<th>Display Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($roles as $role) 
				<tr>
					<td>{{$role->name}}</td>
					<td>{{$role->display_name}}</td>
					<td class="col-sm-1 nowrap">
						<a class="btn {{ env('BTN_EDIT') }} btn-xs" href="{{ action('RoleController@edit', $role->id) }}" >Edit</a> 
			   			{!! Form::open(array('url' => action('RoleController@destroy', $role->id) , 'class' => 'form-delete')) !!}
			   			{!! Form::hidden('_method', 'delete') !!}
			        		<button type="submit" class="btn {{ env('BTN_DELETE') }} btn-xs">Delete</button>
			    		{!! Form::close() !!}
			    	</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('layout.pagination', ['page' => $roles])
@stop