<?php

namespace POS\Http\Controllers;

use Illuminate\Http\Request;

use POS\Http\Requests;
use POS\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('home.index');
    }

}
