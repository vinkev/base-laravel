<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin'], function(){

	Route::get('', ['as' => 'auth/login', 'uses' => 'Auth\AuthController@getLogin']);
	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

	Route::group(['middleware' => 'auth'], function(){
		Route::get('', 'HomeController@index');

		Route::resource('users', 'UserController');
		Route::resource('roles', 'RoleController', ['except' => 'show']);
	});

});

