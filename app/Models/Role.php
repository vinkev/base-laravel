<?php namespace POS\Models;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * POS\Models\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('auth.model')[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.permission')[] $perms
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Role whereDeletedAt($value)
 */
class Role extends EntrustRole
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

}