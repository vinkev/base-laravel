<?php namespace POS\Models;

use Zizaco\Entrust\EntrustPermission;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * POS\Models\Permission
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\Permission whereDeletedAt($value)
 */
class Permission extends EntrustPermission
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
}