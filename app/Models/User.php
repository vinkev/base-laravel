<?php

namespace POS\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * POS\Models\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\POS\Models\User whereUpdatedAt($value)
 */
class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use EntrustUserTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
