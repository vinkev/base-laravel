var elixir = require('laravel-elixir');

/*
 |----------------------------------------------------------------
 | Have a Drink
 |----------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic
 | Gulp tasks for your Laravel application. Elixir supports
 | several common CSS, JavaScript and even testing tools!
 |
 */

elixir(function(mix) {
    mix.styles([
        "bootstrap-3.3.5/css/bootstrap.min.css",
        "bootstrap-simplex.min.css",
        "layout.css"
    ], 'public/css/app.css');

    mix.scripts([
        "jquery-2.1.4.min.js",
        "bootstrap.min.js",
        "main.js"
    ], 'public/js/app.js');

    mix.copy('resources/assets/css/bootstrap-3.3.5/fonts', 'public/fonts');
    mix.copy('resources/assets/css/webfont/Lato', 'public/css/webfont/Lato');
    mix.copy('resources/assets/images', 'public/images');
});
